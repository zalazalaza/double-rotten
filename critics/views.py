from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import CriticRegisterForm
from django.contrib.auth.decorators import login_required


def register(request):
    if request.method == 'POST':
        form = CriticRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}!')
            return redirect('login')
            
    else:
        form = CriticRegisterForm()
    return render(request, 'critics/register.html', {'form':form})

@login_required
def profile(request):
    return render(request, 'critics/profile.html')

