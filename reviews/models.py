import lxml, requests
from bs4 import BeautifulSoup
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

###VALIDATORS###

def good_url(url):
    URLS = ["https://rateyourmusic.com", "https://rottentomatoes.com", "https://rogerebert.com/reviews/", "https://www.rogerebert.com/reviews"]
    if not url.startswith(tuple(URLS)):
        print("fuck ya buddy it worked real good")
        raise ValidationError('URL is not valid target')
    else:
        response = requests.get(url)
        print(response.status_code)
        if response.status_code != 200:
            print("tested url successfully and found a failure")
            raise ValidationError("check that link again bud")

def get_ebert_preview(url):
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    meta_review_text = ""
    headers = {
        "Accpet-Language":"en"
            }
    meta_review = requests.get(url, headers=headers)
    soup = BeautifulSoup(meta_review.text, "lxml")
    meta_review_film_image_link = soup.find(class_="page-content--primary-image").find('img')['src']
    meta_review_critic_name = soup.find(class_="contributor-bio--text").find(class_="contributor-bio--name").text
    meta_review_critic_bio = soup.find(class_="contributor-bio--text").find('p').text
    meta_review_critic_imagelink = soup.find(class_="contributor-bio--image")
    meta_review_paragraphs = soup.find_all(class_="page-content--block_editor-content")
    for paragraph in meta_review_paragraphs:
        meta_review_text += paragraph.text
    print("it worked again")
    return [meta_review_film_image_link, meta_review_critic_name, meta_review_critic_bio, meta_review_critic_imagelink, meta_review_text]



class Review(models.Model):
    title = models.CharField(max_length=100)
    url = models.CharField(max_length=500, default="", validators=[good_url])
    review = models.TextField()
    subject = models.CharField(max_length=20)
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)        
    meta_critic_name = models.CharField(max_length=100, default="bill williams was here")
    meta_critic_bio = models.TextField(default="oh you care now, huh?")
    meta_critic_image = models.CharField(max_length=500, default="nope")
    meta_review_image = models.CharField(max_length=500, default="None")
    meta_review = models.TextField(default="henlo")


    def __str__(self):
        return self.title 

    def get_absolute_url(self):
        return reverse('review-detail', kwargs={'pk': self.pk})

    def save(self, *args, **kwargs):
        print(self.url, "down at the bottom")
        if self.url.startswith("https://rogerebert.com") or self.url.startswith("https://www.rogerebert.com"):
            print("the final zone", self.url)
            print(get_ebert_preview(self.url)[0])
        else:
            pass
        super().save(*args, **kwargs)


class Comment(models.Model):
    post = models.ForeignKey(Review, related_name="comments",  on_delete=models.CASCADE)  
    name = models.ForeignKey(User, on_delete=models.CASCADE)
    body = models.TextField()
    date_added = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '%s - %s' % (self.post.title, self.name)
    
    def save(self, *args, **kwargs):
        print("checkthis out!!!!", self.post.id)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('review-detail', kwargs={'pk': self.post.id})

# Create your models here.
