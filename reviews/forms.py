from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class ReviewForm(forms.Form):
    title = forms.CharField()
    url = forms.CharField()
    subject = forms.CharField(label="Subjecterino", widget=forms.Select(choices=['Film','Music','Vidya','Art']))
    review = forms.CharField()

