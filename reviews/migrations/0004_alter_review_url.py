# Generated by Django 4.0.5 on 2022-07-19 10:34

from django.db import migrations, models
import reviews.models


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0003_alter_review_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='url',
            field=models.CharField(default='This field is required', max_length=500, validators=[reviews.models.good_url]),
        ),
    ]
