from django.urls import path
from .views import ReviewListView, ReviewDetailView, ReviewCreateView, ReviewUpdateView, ReviewDeleteView, UserReviewListView, CommentCreateView, CommentDeleteView
from . import views

urlpatterns = [
    path('user/<str:username>/', UserReviewListView.as_view(), name='user-reviews'),    
    path('reviews/<int:pk>/', ReviewDetailView.as_view(), name='review-detail'),
    path('reviews/<int:pk>/update/', ReviewUpdateView.as_view(), name='review-update'),
    path('reviews/<int:pk>/delete/', ReviewDeleteView.as_view(), name='review-delete'),
    path('reviews/new/', ReviewCreateView.as_view(), name='review-create'),
    path('reviews/<int:pk>/comment', CommentCreateView.as_view(), name='review-comment'),
    path('comment/<int:pk>/delete', CommentDeleteView.as_view(), name='comment-delete' ),
    path('', ReviewListView.as_view(), name='reviews-home'),
    path('about/', views.about, name='reviews-about'),
]
